import "package:flutter/material.dart";

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Form App"),
          backgroundColor: Colors.orange,
        ),
        body: CustomForm(),
      ),
    );
  }
}

class CustomForm extends StatefulWidget {
  const CustomForm({Key? key}) : super(key: key);

  @override
  _CustomFormState createState() => _CustomFormState();
}

class _CustomFormState extends State<CustomForm> {
  final _keyForm = GlobalKey<FormState>();
  FormData data = new FormData();

  @override
  Widget build(BuildContext context) {
    List<Widget> formInputs = [
      TextFormField(
        autofocus: true,
        decoration: InputDecoration(
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
            icon: Icon(Icons.people),
            hintText: "Full name",
            labelText: "Full name"),
        validator: emptyCheck,
        onChanged: (value) => changeState(value, "text"),
      ),
      Container(
        margin: EdgeInsets.symmetric(vertical: 20),
        child: TextFormField(
          obscureText: true,
          decoration: InputDecoration(
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
              icon: Icon(Icons.security),
              hintText: "Password",
              labelText: "Password"),
          validator: emptyCheck,
        ),
      ),
      Container(
        margin: EdgeInsets.only(bottom: 16),
        child: CheckboxListTile(
          value: data.nilaiCheckBox,
          title: Text("Programming"),
          subtitle: Text("Memiliki kemampuan programming"),
          onChanged: (value) => changeState(value, "checkbox"),
        ),
      ),
      Container(
        margin: EdgeInsets.only(bottom: 16),
        child: SwitchListTile(
          value: data.nilaiSwitch,
          title: Text("Backend Programming"),
          subtitle: Text("Java Spring/Node JS/PHP Laravel"),
          onChanged: (value) => changeState(value, "switch"),
        ),
      ),
      Container(
        padding: EdgeInsets.only(left: 15.5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Working Experience", style: TextStyle(fontSize: 16)),
            Slider(
              label: "${data.nilaiSlider.toInt().toString()} tahun",
              value: data.nilaiSlider,
              divisions: 15,
              min: 0,
              max: 15,
              onChanged: (value) => changeState(value, "slider"),
            ),
          ],
        ),
      ),
      ElevatedButton(onPressed: submitForm, child: Text("Submit"))
    ];

    return Form(
      key: _keyForm,
      child: ListView(
          padding: EdgeInsets.only(left: 16, right: 16, top: 24),
          children: [
            Column(
              children: formInputs,
            ),
          ]),
    );
  }

  String? emptyCheck(String? value) {
    if (value!.isEmpty) {
      return "Please fill the field";
    }
    return null;
  }

  void submitForm() {
    String result =
        "Name : ${data.name}\nPassword : ${data.password}\nProgramming : ${data.nilaiCheckBox}\nBackend : ${data.nilaiSwitch}\nWorking Experience : ${data.nilaiSlider.toInt().toString()}";

    if (_keyForm.currentState!.validate()) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Success")));

      showDialog(
          context: context,
          builder: (BuildContext context) => AlertDialog(
                title: Text("Result"),
                content: Text(result),
              ));
    }
  }

  void changeState(Object? value, String type) {
    setState(() {
      switch (type) {
        case "text":
          data.name = value.toString();
          break;
        case "checkbox":
          data.nilaiCheckBox = value as bool;
          break;

        case "switch":
          data.nilaiSwitch = value as bool;
          break;

        case "slider":
          data.nilaiSlider = value as double;
          break;

        case "password":
          data.password = value.toString();
          break;
        default:
          return null;
      }
    });
  }
}

class FormData {
  String name = "";
  String password = "";
  double nilaiSlider = 0;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = false;
}
